package com.example.calcpestelle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.logo_top_left);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    public void Leave(View view) {
        finish();
        System.exit(0);
    }

    public void Raz(View view) {
        ((EditText)findViewById(R.id.Value1)).getText().clear();
        ((EditText)findViewById(R.id.Value2)).getText().clear();
        ((TextView)findViewById(R.id.Result)).setText("");
    }

    public void Calculate(View view) {
        double firstValue = Double.parseDouble(((EditText) findViewById(R.id.Value1)).getText().toString());
        double secondValue = Double.parseDouble(((EditText) findViewById(R.id.Value2)).getText().toString());

        double result = 0.0D;

        switch (((RadioGroup)findViewById(R.id.Operator)).getCheckedRadioButtonId())
        {
            case R.id.RadioPlus:
                result = firstValue + secondValue;
                break;

            case R.id.RadioMoins:
                result = firstValue - secondValue;
                break;

            case R.id.RadioDiviser:
                result = firstValue / secondValue;
                break;

            case R.id.RadioMultiplier:
                result = firstValue * secondValue;
                break;

            default:
                ((TextView)findViewById(R.id.Result)).setText("");
                break;
        }

        ((TextView) findViewById(R.id.Result)).setText("" + result);
    }
}